# -*- coding: utf-8 -*-
##
# Importing section
import random
##


class Weapon(object):
    ##
    def __init__(self, name="", description="", damage=1, protection=1):
        pass
        self.name = name
        self.description = description
        self.damage = damage
        self.protection = protection
        self.effects = Effects()
    ##


class User(object):
    ##
    def __init__(self):
        pass
        self.money = {'gold': 0, 'silver':0, 'copper':20}

        self.maxHealth = 100
        self.health = self.maxHealth
        self.maxMana = 100
        self.mana = self.maxMana
        self.fatigue = 0
        self.maxFatigue = 100
        self.maxStamina = 100
        self.stamina = self.maxStamina
        self.hunger = 0
        self.maxHunger = 100
        self.exp = 0
        self.maxExp = 125
        self.level = 1

        self.definedInventory = {'additionalTrinket': 0, 'back': 0, 'belt': 0,
                                'feet': 0, 'head': 0, 'leftArm': 0,
                                'leftHand': 0, 'leftHandTrinket': 0, 'legs': 0,
                                'mainHand': 0, 'necklace': 0, 'offHand': 0,
                                'rightArm': 0, 'rightHand': 0, 'rightHandTrinket': 0,
                                'torso': 0}
        self.undefinedInventory = [None] * 18

        self.stats = {"strength": random.randrange(1,5,1), "defense": random.randrange(1,5,1), "speed": random.randrange(1,5,1),
                      "attackMagic": random.randrange(1, 5, 1), "defenseMagic": random.randrange(1,5,1), "specialMagic": random.randrange(1,5,1)}

        self.strength = random.randrange(1,5,1)
        self.speed = random.randrange(1,5,1)
        self.defense = random.randrange(1,5,1)
        self.attackMagic = random.randrange(1,5,1)
        self.defenseMagic = random.randrange(1,5,1)
        self.specialMagic = random.randrange(1,5,1)

        if random.randrange(0,1000,1) == 666:
            randv = random.randrange(0,10,1)
        else:
            randv = 0

        self.magicAffinity = {'Fire': random.randrange(0,10,1), 'Water': random.randrange(0,10,1),
                              'Earth': random.randrange(0,10,1), 'Air': random.randrange(0,10,1),
                              'Light': random.randrange(0,10,1), 'Darkness': random.randrange(0,10,1),
                              'Void': randv}

        self.skills = []
        self.sp = 0

        self.skilled = {'health': 0, 'mana': 0, 'fatigue': 0, 'stamina': 0, 'hunger': 0, 'strength': 0,
                        'speed': 0, 'defense': 0, 'attackMagic': 0, 'defenseMagic': 0, 'specialMagic': 0}

        self.inFight = False
        self.enemy = ""
        self.enemyLevel = None
        self.enemyIsBoss = False
        self.isFirst = True
        self.turn = 0
        self.canAttack = False

        self.partyId = None
        self.inParty = False
        self.party = [None] * 5

        self.mobile = False
    ##


class Effects(object):
    ##
    def __init__(self, poison=False, burn=False, freeze=False, petrify=False, bless=False, curse=False):
        pass
        self.poison = poison
        self.burn = burn
        self.freeze = freeze
        self.petrify = petrify
        self.bless = bless
        self.curse = curse
    ##


class EnemyList(object):
    ##
    def __init__(self):
        pass

        ls_slime = {"rarity": "[Common]", "race": "Slime", "url": "https://puu.sh/xIGd6/ca92c1c27e.png"}
        ls_goblin = {"rarity": "[Common]", "race": "Goblin", "url": "https://puu.sh/xItGW/b02aad4c05.png"}
        ls_bandit = {"rarity": "[Common]", "race": "Bandit", "url": "https://puu.sh/xIIDb/54b792417e.png"}
        ls_boar = {"rarity": "[Common]", "race": "Boar", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_bear = {"rarity": "[Common]", "race": "Bear", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_wolf = {"rarity": "[Common]", "race": "Wolf", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_skeleton = {"rarity": "[Common]", "race": "Skeleton", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_spider = {"rarity": "[Common]", "race": "Spider", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_snake = {"rarity": "[Common]", "race": "Snake", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_gremlin = {"rarity": "[Common]", "race": "Gremlin", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_kobold = {"rarity": "[Common]", "race": "Kobold", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        self.common =   [ls_slime, ls_goblin, ls_bandit, ls_boar, ls_bear, ls_wolf, ls_skeleton, ls_spider,
                         ls_snake, ls_gremlin, ls_kobold]

        ls_gsnake = {"rarity": "[Uncommon]", "race": "Giant Snake", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_gant = {"rarity": "[Uncommon]", "race": "Giant Ant", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_grat = {"rarity": "[Uncommon]", "race": "Giant Rat", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_gbee = {"rarity": "[Uncommon]", "race": "Giant Bee", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_gscorpion = {"rarity": "[Uncommon]", "race": "Giant Scorpion", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_troll = {"rarity": "[Uncommon]", "race": "Troll", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_werewolf = {"rarity": "[Uncommon]", "race": "Werewolf", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_demon = {"rarity": "[Uncommon]", "race": "Demon", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        self.uncommon = [ls_gsnake, ls_gant, ls_grat, ls_gbee, ls_gscorpion, ls_troll, ls_werewolf, ls_demon]

        ls_golem = {"rarity": "[Rare]", "race": "Golem", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_gryphon = {"rarity": "[Rare]", "race": "Gryphon", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_dlord = {"rarity": "[Rare]", "race": "Demon Lord", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        self.rare = [ls_golem, ls_gryphon, ls_dlord]

        ls_dragon = {"rarity": "[Boss]", "race": "Dragon", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_hydra = {"rarity": "[Boss]", "race": "Hydra", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_angel = {"rarity": "[Boss]", "race": "Angel", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        ls_dking = {"rarity": "[Boss]", "race": "Demon King", "url": "https://puu.sh/xIusJ/65e3ea1235.png"}
        self.boss = [ls_dragon, ls_hydra, ls_angel, ls_dking]

    def common(self):
        return self.common

    def uncommon(self):
        return self.uncommon

    def rare(self):
        return self.rare

    def boss(self):
        return self.boss
